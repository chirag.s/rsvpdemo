package com.rsvp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * The Class RsvpExcelToMySqlApplicationTests.
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class RsvpExcelToMySqlApplicationTests {

  /**
   * Context loads.
   */
  //@Test
  public void contextLoads() {
    RsvpExcelToMySqlApplication.main(new String[] {});
  }

}
