package com.rsvp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.rsvp.RsvpExcelToMySqlApplication;
import com.rsvp.repository.EventUserRepository;

/**
 * The Class ParticipantsControllerTest.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = RsvpExcelToMySqlApplication.class)
//@SpringBootTest
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RSVPControllerTest {

  /** The mock mvc. */
  private MockMvc mockMvc;

  /** The wac. */
  //@Autowired
  private WebApplicationContext wac;

  /** The participant service. */
  //@MockBean
  private EventUserRepository eventUserRepository;

  /**
   * Setup.
   */
  //@Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  /**
   * Convert test.
   *
   * @throws Exception the exception
   */
  // @Test
  public void convertTest() throws Exception {
    mockMvc.perform(get("/convertExcel")).andExpect(status().isOk());
  }

}
