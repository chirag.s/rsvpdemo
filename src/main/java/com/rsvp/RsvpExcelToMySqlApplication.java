package com.rsvp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class RsvpExcelToMySqlApplication.
 */
@SpringBootApplication
public class RsvpExcelToMySqlApplication {

  /**
   * The main method.
   *
   * @param args the arguments
   */
  public static void main(final String[] args) {
    SpringApplication.run(RsvpExcelToMySqlApplication.class, args);
  }

}
