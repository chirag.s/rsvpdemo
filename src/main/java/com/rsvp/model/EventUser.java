package com.rsvp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class ParticipantsModel.
 */
@Entity
@Table(name = "eventuser")
public class EventUser implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4138124745460057633L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id")
  private Long userId;

  /** The first name. */
  @Column(name = "first_name")
  private String firstName;

  /** The last name. */
  @Column(name = "last_name")
  private String lastName;

  /** The dob. */
  @Column(name = "date_of_birth")
  @Temporal(TemporalType.DATE)
  private Date dob;

  /** The email address. */
  @Column(unique = true, name = "email_address")
  private String emailAddress;

  /** The phone number. */
  @Column(name = "phone_number")
  private String phoneNumber;

  /** The language. */
  @Column(name = "prefered_language")
  private String language;

  /** The date of interest. */
  @Column(name = "date_of_interest")
  @Temporal(TemporalType.DATE)
  private Date dateOfInterest;

  /** The status. */
  @Column(name = "status")
  private boolean status = false;

  /** The city. */
  @Column(name = "city_name")
  private String cityName;

  /**
   * Gets the first name.
   *
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * Gets the user id.
   *
   * @return the user id
   */
  public Long getUserId() {
    return userId;
  }

  /**
   * Sets the user id.
   *
   * @param u the new user id
   */
  public void setUserId(final Long u) {
    this.userId = u;
  }

  /**
   * Sets the first name.
   *
   * @param firstNameVal the firstName to set
   */
  public void setFirstName(final String firstNameVal) {
    firstName = firstNameVal;
  }

  /**
   * Gets the last name.
   *
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * Sets the last name.
   *
   * @param lastNameVal the lastName to set
   */
  public void setLastName(final String lastNameVal) {
    lastName = lastNameVal;
  }

  /**
   * Gets the dob.
   *
   * @return the dob
   */
  public Date getDob() {
    if (dob == null) {
      return null;
    } else {
      return (Date) dob.clone();
    }
  }

  /**
   * Sets the dob.
   *
   * @param dobVal the dob to set
   */
  public void setDob(final Date dobVal) {
    if (dobVal == null) {
      this.dob = null;
    } else {
      this.dob = (Date) dobVal.clone();
    }
  }

  /**
   * Gets the email address.
   *
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * Sets the email address.
   *
   * @param emailAddressVal the emailAddress to set
   */
  public void setEmailAddress(final String emailAddressVal) {
    emailAddress = emailAddressVal;
  }

  /**
   * Gets the phone number.
   *
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Sets the phone number.
   *
   * @param phoneNumberVal the phoneNumber to set
   */
  public void setPhoneNumber(final String phoneNumberVal) {
    phoneNumber = phoneNumberVal;
  }

  /**
   * Gets the language.
   *
   * @return the language
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Sets the language.
   *
   * @param languageVal the language to set
   */
  public void setLanguage(final String languageVal) {
    language = languageVal;
  }

  /**
   * Gets the date of interest.
   *
   * @return the dateOfInterest
   */
  public Date getDateOfInterest() {
    if (dateOfInterest == null) {
      return null;
    } else {
      return (Date) dateOfInterest.clone();
    }
  }

  /**
   * Sets the date of interest.
   *
   * @param dateOfInterestVal the dateOfInterest to set
   */
  public void setDateOfInterest(final Date dateOfInterestVal) {
    if (dateOfInterestVal == null) {
      this.dateOfInterest = null;
    } else {
      this.dateOfInterest = (Date) dateOfInterestVal.clone();
    }
  }

  /**
   * @return the status
   */
  public final boolean isStatus() {
    return status;
  }

  /**
   * Sets the status.
   *
   * @param s the new status
   */
  public final void setStatus(final boolean s) {
    this.status = s;
  }

  /**
   * @return the city
   */
  public final String getCity() {
    return cityName;
  }

  /**
   * Sets the city.
   *
   * @param c the new city
   */
  public final void setCity(final String c) {
    this.cityName = c;
  }

}
