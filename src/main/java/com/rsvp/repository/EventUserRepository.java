package com.rsvp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rsvp.model.EventUser;

/**
 * The Interface EventUserRepository.
 */
@Repository("eventUserRepository")
public interface EventUserRepository extends JpaRepository<EventUser, Long> {

}
