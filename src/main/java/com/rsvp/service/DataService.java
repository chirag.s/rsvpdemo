package com.rsvp.service;

import java.util.List;

import com.rsvp.model.EventUser;

/**
 * The Interface DataService.
 */
public interface DataService {

  /**
   * Gets the event users.
   *
   * @return the event users
   */
  List<EventUser> getEventUsers();

}
