package com.rsvp.service;

import java.util.List;

import com.rsvp.model.EventUser;

/**
 * The Interface EventUserService.
 */
public interface EventUserService {

  /**
   * Save.
   *
   * @param users the users
   */
  void save(List<EventUser> users);

}
