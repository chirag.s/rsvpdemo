package com.rsvp.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.rsvp.model.EventUser;
import com.rsvp.service.DataService;
import com.rsvp.utils.AppConstants;

/**
 * The Class DataServiceImpl.
 */
@Service("dataService")
public class DataServiceImpl implements DataService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LogManager.getLogger("Logger");

  /*
   * (non-Javadoc)
   * @see com.rsvp.service.DataService#getEventUsers()
   */
  @Override
  public List<EventUser> getEventUsers() {
    List<EventUser> eventUsers = new ArrayList<>();
    try (InputStream excelFile = new FileInputStream(
        new ClassPathResource("revp_data.xlsx").getFile())) {
      XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
      XSSFSheet datatypeSheet = workbook.getSheetAt(0);
      Iterator<Row> iterator = datatypeSheet.iterator();
      while (iterator.hasNext()) {

        Row currentRow = iterator.next();
        if (currentRow.getRowNum() == 0) {
          continue;
        }
        Iterator<Cell> cellIterator = currentRow.iterator();

        EventUser user = new EventUser();
        while (cellIterator.hasNext()) {

          Cell currentCell = cellIterator.next();
          if (currentCell.getColumnIndex() == AppConstants.ZERO) {
            user.setDateOfInterest(currentCell.getDateCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.ONE) {
            user.setFirstName(currentCell.getStringCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.TWO) {
            user.setLastName(currentCell.getStringCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.THREE) {
            user.setDob(currentCell.getDateCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.FOUR) {
            user.setEmailAddress(currentCell.getStringCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.FIVE) {
            currentCell.setCellType(CellType.STRING);
            user.setPhoneNumber(currentCell.getStringCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.SIX) {
            user.setCity(currentCell.getStringCellValue());
          } else if (currentCell.getColumnIndex() == AppConstants.SEVEN) {
            user.setLanguage(currentCell.getStringCellValue());
          }
        }
        eventUsers.add(user);
      }
      workbook.close();
    } catch (IOException e) {
      LOGGER.error(ExceptionUtils.getStackTrace(e));
    }
    return eventUsers;
  }

}
