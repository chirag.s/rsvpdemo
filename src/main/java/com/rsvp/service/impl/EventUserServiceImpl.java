package com.rsvp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsvp.model.EventUser;
import com.rsvp.repository.EventUserRepository;
import com.rsvp.service.EventUserService;

/**
 * The Class EventUserServiceImpl.
 */
@Service("eventUserService")
public class EventUserServiceImpl implements EventUserService {

  /** The event user repository. */
  @Autowired
  private EventUserRepository eventUserRepository;

  /*
   * (non-Javadoc)
   * @see com.rsvp.service.EventUserService#save(java.util.List)
   */
  @Override
  public void save(final List<EventUser> users) {
    eventUserRepository.save(users);
  }

}
