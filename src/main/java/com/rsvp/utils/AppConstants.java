package com.rsvp.utils;

/**
 * The Class AppConstants.
 */
public final class AppConstants {

  /** The success. */
  public static final String SUCCESS = "Success";

  /** The fail. */
  public static final String FAIL = "Fail";

  /** The Constant ZERO. */
  public static final int ZERO = 0;

  /** The Constant ONE. */
  public static final int ONE = 1;

  /** The Constant TWO. */
  public static final int TWO = 2;

  /** The Constant THREE. */
  public static final int THREE = 3;

  /** The Constant FOUR. */
  public static final int FOUR = 4;

  /** The Constant FIVE. */
  public static final int FIVE = 5;

  /** The Constant SIX. */
  public static final int SIX = 6;

  /** The Constant SEVEN. */
  public static final int SEVEN = 7;

  /** Instantiates a new app constants. */
  private AppConstants() {
    //
  }

  /**
   * Instance.
   *
   * @return the app constants
   */
  public static AppConstants instance() {
    return new AppConstants();
  }

}
