package com.rsvp.utils;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class GlobalUtilityHandler.
 */
public final class GlobalUtilityHandler {

  /**
   * log4j.Logger.
   */
  private static final Logger LOGGER = LogManager.getLogger("Logger");

  /**
   * Instantiates a new global utility.
   */
  private GlobalUtilityHandler() {
    /*
     * Private Constructor
     */
  }

  /**
   * Instance.
   *
   * @return the app constants
   */
  public static GlobalUtilityHandler instance() {
    return new GlobalUtilityHandler();
  }

  /**
   * Convert class to json.
   *
   * @param input the input
   * @return the string
   * @throws JsonProcessingException the json processing exception
   */
  public static String convertClassToJson(final Object input) {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setSerializationInclusion(Include.NON_NULL);
    try {
      return objectMapper.writeValueAsString(input);
    } catch (JsonProcessingException e) {
      LOGGER.error(ExceptionUtils.getStackTrace(e));
    }
    return null;
  }
}
