package com.rsvp.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rsvp.model.EventUser;
import com.rsvp.service.DataService;
import com.rsvp.service.EventUserService;
import com.rsvp.utils.GlobalUtilityHandler;

/**
 * The Class RSVPController.
 */
@RestController
public class RSVPController {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LogManager.getLogger("Logger");

  /** The data conversion service. */
  @Autowired
  private DataService dataService;

  /** The event user service. */
  @Autowired
  private EventUserService eventUserService;

  /**
   * Convert CSV data to my SQL.
   *
   * @return the string
   */
  @GetMapping("/convertExcel")
  public String convertCSVDataToMySQL() {
    LOGGER.info("convertCSVDataToMySQL() called");
    List<EventUser> users = dataService.getEventUsers();
    LOGGER.info("Event User List : " + GlobalUtilityHandler.convertClassToJson(users));
    eventUserService.save(users);
    LOGGER.info("convertCSVDataToMySQL() end..");
    return "Convertintg CSV Data To MySQL";
  }

}
